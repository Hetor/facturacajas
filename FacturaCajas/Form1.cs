﻿using Microsoft.Vbe.Interop;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacturaCajas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
        }

        string CarpetaString = "";

        #region Botones       
        private void btnCarpeta_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns.Clear();

            dataGridView1.Columns.Add("TRAILERS", "TRAILERS");
            dataGridView1.Columns.Add("DATE", "DATE");
            dataGridView1.Columns.Add("DATEND", "DATEND");
            dataGridView1.Columns.Add("DAYS", "DAYS");
            dataGridView1.Columns.Add("RESPONSBLE", "RESPONSBLE");
            dataGridView1.Columns.Add("MATERIAL", "MATERIAL");
            dataGridView1.Columns.Add("CODE", "CODE");

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            dataGridView2.Columns.Clear();

            Carpeta();

            try
            {
                string[] words = dataGridViewLista.Rows[0].Cells[0].Value.ToString().Split();

                String Tetsto = words[0] + " " + words[1];
                if (TbxNombre.Text != Tetsto)
                {
                    DialogResult dialogResult = MessageBox.Show($"El nombre del documento no coincide con {TbxNombre.Text} deseas sustituirlo?", "Alerta", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                        BitacoraCajas.Properties.Settings.Default.NameDoc = Tetsto;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Lista();
            dataGridView1.Sort(this.dataGridView1.Columns["TRAILERS"], ListSortDirection.Ascending);
            //dataGridView1.Sort(this.dataGridView1.Columns["Trailers"], ListSortDirection.Ascending);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //dataGridView1.Columns.Add("DAYS", "DAYS");
            //DateTime d = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, 01);

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1[2, i].Value.ToString() == "")
                    dataGridView1[2, i].Value = dateTimePicker1.Value;

                //DateTime dInicio = DateTime.ParseExact(dataGridView1[1, i].Value.ToString(), "MM/dd/yyyy HH:mm:ss tt", null);
                //DateTime dCorte = DateTime.ParseExact(dataGridView1[2, i].Value.ToString(), "MM/dd/yyyy HH:mm:ss tt", null);

                DateTime dInicio = Convert.ToDateTime(dataGridView1[1, i].Value);
                DateTime dCorte = Convert.ToDateTime(dataGridView1[2, i].Value);

                TimeSpan tSpan = dCorte - dInicio.AddDays(6);
                int dias = tSpan.Days;
                //int dias = Convert.ToInt32(dCorte - dInicio.AddDays(7));

                if (dias > dateTimePicker1.Value.Day)
                    dataGridView1[3, i].Value = dateTimePicker1.Value.Day;
                else if (dias < 0)
                    dataGridView1[3, i].Value = 0;
                else if (dias > dCorte.Day)
                    dataGridView1[3, i].Value = dCorte.Day;
                else
                    dataGridView1[3, i].Value = dias;

            }

            Guardar();
            MessageBox.Show("Guardado");
        }
        #endregion

        #region Eventos
        private void Form1_Load(object sender, EventArgs e)
        {
            TbxNombre.Text = BitacoraCajas.Properties.Settings.Default.NameDoc;
        }
        private void TbxNombre_TextChanged(object sender, EventArgs e)
        {
            if (TbxNombre.Text != BitacoraCajas.Properties.Settings.Default.NameDoc)
            {
                DialogResult dialogResult = MessageBox.Show($"Deseas sustituirlo el texto a {TbxNombre.Text}", "Alerta", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                    BitacoraCajas.Properties.Settings.Default.NameDoc = TbxNombre.Text;
                if (dialogResult == DialogResult.No)
                    TbxNombre.Text = BitacoraCajas.Properties.Settings.Default.NameDoc;
            }            
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tome en cuenta:" +
                "\n 1.Formato de nombre *Copia de Reporte Trailers mm/dd/aaaa*. \n" +
                "\n 2.Revisar el orden de la lista de archivos despues de *SELECCIONAR CARPETA*. \n" +
                "\n 3. Revisar la fecha de corte antes de *GUARDAR*. \n" +
                "\n Dindo Dia!  ¯\\_(ツ)_/¯");
        }
        #endregion

        public void Carpeta()
        {
            dataGridViewLista.Rows.Clear();

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                int N = 0;
                CarpetaString = folderBrowserDialog1.SelectedPath;
                DirectoryInfo Dir = new DirectoryInfo(@"" + CarpetaString);

                foreach (var file in Dir.GetFiles("*.xlsx", SearchOption.AllDirectories))
                {
                    N++;
                    DataGridViewRow row = (DataGridViewRow)dataGridViewLista.Rows[0].Clone();
                    row.Cells[0].Value = file;
                    dataGridViewLista.Rows.Add(row);
                }
                lblNumero.Text = "Lista Exel " + N.ToString();
            }

            dataGridViewLista.Sort(this.dataGridViewLista.Columns["Archivos"], ListSortDirection.Ascending);
        }

        public void Lista()
        {
            int h = 0;
            try
            {
                //Copia el primer Excel
                dataGridView2.DataSource = ImportarDatos(CarpetaString + "\\" + dataGridViewLista.Rows[h].Cells[0].Value, "Trailers");

                for (int i = 0; i < dataGridView2.Rows.Count; i++)
                {
                    if (Convert.ToString(dataGridView2[0, i].Value).Replace(" ", String.Empty) != "" && Convert.ToString(dataGridView2[0, i].Value).Replace(" ", String.Empty) != null)
                    {
                        dataGridView1.Rows.Add(new string[] {
                            Convert.ToString(dataGridView2[0, i].Value).Replace(" ", String.Empty),
                            Convert.ToString(dataGridView2[1, i].Value),
                            "",
                            "",
                            Convert.ToString(dataGridView2[2, i].Value),
                            Convert.ToString(dataGridView2[3, i].Value),
                            Convert.ToString(dataGridView2[4, i].Value)//SeCambio
                        });
                    }
                }
                //dataGridView1.Sort(this.dataGridView1.Columns["DATE"], ListSortDirection.Ascending);
                dataGridViewLista.Rows[0].Cells[0].Style.BackColor = Color.Green;

                //Todos los demas Excel
                for (h = 1; h < dataGridViewLista.RowCount - 1; h++)
                {
                    dataGridView2.DataSource = ImportarDatos(CarpetaString + "\\" + dataGridViewLista.Rows[h].Cells[0].Value, "Trailers");

                    string NombreDocumento = BitacoraCajas.Properties.Settings.Default.NameDoc;

                    string FH = Convert.ToString(dataGridViewLista.Rows[h].Cells[0].Value).Replace(NombreDocumento, String.Empty);

                    FH = FH.Replace(".xlsx", String.Empty);
                    FH = FH.Replace(".XLSX", String.Empty);
                    FH = FH.Replace(".xls", String.Empty);
                    FH = FH.Replace(".XLS", String.Empty);

                    FH = FH.Replace(" ", String.Empty);
                    FH = FH.Replace("-", "/");

                    string[] words = FH.Split();
                    FH = words[words.Length - 1];

                    string[] Fecha = FH.Split('/');

                    if (Fecha[2].Length > 2)
                    {
                        Fecha[2] = Fecha[2].Replace("20", "");
                    }

                    DateTime Tiempo = new DateTime(Convert.ToInt32(20 + Fecha[2]), Convert.ToInt32(Fecha[0]), Convert.ToInt32(Fecha[1]));
                    FH = Tiempo.ToString("MM/dd/yyyy");

                    //Tabla 1 Revisa Retirados
                    for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                    {
                        for (int j = 0; j < dataGridView2.Rows.Count; j++)
                        {
                            //Si ya esta cerrado
                            if (dataGridView1[2, i].Value.ToString() != "")
                                break;

                            //Si existe
                            if (Convert.ToString(dataGridView1[0, i].Value) == Convert.ToString(dataGridView2[0, j].Value).Replace(" ", String.Empty))
                                break;

                            //Si no esta cerrado
                            if (j == dataGridView2.Rows.Count - 1)
                                dataGridView1[2, i].Value = FH;
                        }
                    }

                    ////Tabla 2 Agrega nueva o quita con fecha diferente
                    for (int i = 0; i < dataGridView2.Rows.Count; i++)
                    {
                        for (int j = 0; j < dataGridView1.Rows.Count - 1; j++)
                        {
                            //Si Trailers2 == Trailes1 
                            if (Convert.ToString(dataGridView2[0, i].Value).Replace(" ", String.Empty) == Convert.ToString(dataGridView1[0, j].Value))
                            {
                                //Si Fecha2 == Fecha1
                                if (dataGridView2[1, i].Value.ToString() == dataGridView1[1, j].Value.ToString())
                                {
                                    break;
                                }
                                //Si Fecha2 != Fecha1 && FechaEnd esta vacio
                                else if (dataGridView2[1, i].Value.ToString() != dataGridView1[1, j].Value.ToString() && dataGridView1[2, j].Value.ToString() == "")
                                {
                                    DateTime DT = Convert.ToDateTime(dataGridView2[1, i].Value).AddDays(-1);

                                    dataGridView1[2, j].Value = DT.ToString();
                                    break;
                                }
                            }
                            //Si no existe y llega al final
                            if (j == dataGridView1.Rows.Count - 2)
                            {
                                //Si no esta basio
                                if (Convert.ToString(dataGridView2[0, i].Value).Replace(" ", String.Empty) != "" && Convert.ToString(dataGridView2[0, i].Value).Replace(" ", String.Empty) != null)
                                {
                                    dataGridView1.Rows.Add(new string[] {
                                        Convert.ToString(dataGridView2[0, i].Value).Replace(" ", String.Empty),
                                        Convert.ToString(dataGridView2[1, i].Value),
                                        "",
                                        "",
                                        Convert.ToString(dataGridView2[2, i].Value),
                                        Convert.ToString(dataGridView2[3, i].Value),
                                        Convert.ToString(dataGridView2[4, i].Value)//SeCambio,                     
                                    });
                                    break;
                                }
                            }
                        }
                    }

                    dataGridViewLista.Rows[h].Cells[0].Style.BackColor = Color.Green;
                    //dataGridView1.Sort(this.dataGridView1.Columns["Date"], ListSortDirection.Ascending);
                }
            }
            catch (Exception)
            {
                dataGridViewLista.Rows[h].Cells[0].Style.BackColor = Color.Red;
                //throw;
            }

        }

        DataView ImportarDatos(string nombrearchivo, string Hoja)
        {
            //string conexion = string.Format("Provider = Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties = 'Excel 12.0;'", nombrearchivo);
            string conexion = String.Format("Provider = Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties = 'Excel 12.0; HDR=Yes'", nombrearchivo);


            OleDbConnection conector = new OleDbConnection(conexion);

            conector.Open();
            OleDbCommand consulta = new OleDbCommand();

            if (Hoja == "Trailers")
                //consulta = new OleDbCommand("SELECT TRAILERS,DATE,Responsable,RMKS,COD FROM[" + Hoja + "$] WHERE CARRIER = 'HUNGAROS' ORDER BY TRAILERS", conector);
                consulta = new OleDbCommand("SELECT TRAILERS,[DATE ARRIVED],Responsable,RMKS,COD FROM[" + Hoja + "$] WHERE CARRIER = 'HUNGAROS' ORDER BY TRAILERS", conector);
            else
                consulta = new OleDbCommand("SELECT * FROM[" + Hoja + "]", conector);


            OleDbDataAdapter adaptador = new OleDbDataAdapter
            {
                SelectCommand = consulta
            };

            DataSet ds = new DataSet();

            adaptador.Fill(ds);

            conector.Close();

            return ds.Tables[0].DefaultView;
        }

        public void Guardar()
        {
            DateTime DT = DateTime.Now;
            String Mes = DT.ToString("MMMM", CultureInfo.CreateSpecificCulture("es"));
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls)|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    #region
                    //Microsoft.Office.Interop.Excel.Application aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    //Microsoft.Office.Interop.Excel.Workbook libros_trabajo = new Microsoft.Office.Interop.Excel.Workbook();
                    //Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo;
                    //aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    //libros_trabajo = aplicacion.Workbooks.Add();
                    //hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    #endregion
                    Microsoft.Office.Interop.Excel.Application aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    Microsoft.Office.Interop.Excel.Workbook libros_trabajo = aplicacion.Workbooks.Add();
                    Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);

                    for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                    {
                        for (int j = 0; j < dataGridView1.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                    libros_trabajo.SaveAs(fichero.FileName,
                       Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    //libros_trabajo.SaveAs(CarpetaString + "\\" + Mes,
                    //Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Error Posiblemente este abierto el Excel");
                MessageBox.Show(eX.Message);
            }
        }
    }
}
